package us.d8u.news;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import au.com.bytecode.opencsv.CSVWriter;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.ParsingFeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;

public class Main {
	private static Logger logger = Logger.getLogger("Main");
	public static final AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifierNoExceptions(getClassifierPath());

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {
		long start = System.currentTimeMillis();
		BufferedReader reader = null;
		final PrintWriter writer_ = new PrintWriter(System.out, true);
		writer_.println("\"Place\"|\"lat\"|\"lng\"|\"Title\"");
		try {
			reader = new BufferedReader(new FileReader("feeds.txt"));
		} catch (FileNotFoundException e) {
			logger.fatal(e.getMessage(), e);
			logger.fatal("Couldn't find feeds, put them in feeds.txt one URL on the line");
			System.exit(-1);
		}
		String feedUrl = null;
		InputSource is = null;
		new File("/tmp/data2.csv").delete();
		while ((feedUrl = reader.readLine()) != null) {
			SyndFeed feedParsingSource = null;
			try {
				feedParsingSource = new SyndFeedInput().build(new XmlReader(new URL(feedUrl)));
			} catch (ParsingFeedException e1) {
				logger.fatal(feedUrl + " failed to parse, skipping");
				continue;
			} catch (IllegalArgumentException e1) {
				logger.fatal(e1.getMessage(), e1);
			} catch (MalformedURLException e1) {
				logger.fatal(e1.getMessage(), e1);
			} catch (FeedException e1) {
				logger.fatal(e1.getMessage(), e1);
			} catch (IOException e1) {
				logger.fatal(e1.getMessage(), e1);
			}

			List<SyndEntry> entries2 = feedParsingSource.getEntries();
			for (SyndEntry entry : entries2) {
				String link = entry.getLink();
				final String title = String.format("<a href=\"%s\">%s</a>",link, entry.getTitle(), link);
				List<String> locations = null;
				try {
					locations = LocationLookup.extractLocations(new URL(link));
				} catch (MalformedURLException e2) {
					logger.fatal(e2.getMessage(), e2);
					continue;
				} catch (SocketTimeoutException e2) {
					logger.fatal(e2.getMessage() + " -- restarting with next entry", e2);
					continue;
				}
				if (locations == null) {
					logger.info("sleeping for a bit, till we can get a connection at Nokia again");
					try {
						Thread.sleep(15 * 1000);
					} catch (InterruptedException e) {
						logger.fatal(e.getMessage(), e);
					}
					continue;
				}
				SAXParser parser = null;
				try {
					parser = SAXParserFactory.newInstance().newSAXParser();
				} catch (ParserConfigurationException e1) {
					logger.fatal(e1.getMessage(), e1);
				} catch (SAXException e1) {
					logger.fatal(e1.getMessage(), e1);
				}
				InputStream osmXml = null;
				for (final String location : locations) {
					HttpURLConnection connection = null;
					try {
						connection = (HttpURLConnection) new URL(String.format(LocationLookup.URL, URLEncoder.encode(location, "utf-8"))).openConnection();
					} catch (MalformedURLException e1) {
						logger.fatal(e1.getMessage(), e1);
					} catch (UnsupportedEncodingException e1) {
						logger.fatal(e1.getMessage(), e1);
					} catch (IOException e1) {
						logger.fatal(e1.getMessage(), e1);
					}
					try {
						osmXml = connection.getInputStream();
					} catch (IOException e1) {
						logger.fatal(e1.getMessage(), e1);
					}
					logger.debug(osmXml);
					is = new InputSource(osmXml);
					DefaultHandler handler = null;
					try {
						handler = new DefaultHandler() {
								CSVWriter writer = null;
								private boolean inPosition = false;
								private String coordinateType = null;
								Double latitude, longitude;
								@Override
								public void characters(char[] ch, int start, int length) {
									if (!inPosition) {
										return;
									}
									if (coordinateType.equals("Latitude")) {
										latitude = Double.parseDouble(new String(ch, start, length));
										logger.debug(latitude + "<=== latitude");
									}
									if (coordinateType.equals("Longitude")) {
										longitude = Double.parseDouble(new String(ch, start, length));
										logger.debug(longitude + "<==== longitude");
									}
								}

								@Override
								public void endElement(String uri, String localName, String qName) {
									if (qName.contains("Position")) {
										inPosition = false;
										if (inRange()) {
											String[] line = new String[] { location, latitude.toString(), longitude.toString(), title };
											writer.writeNext(line);
										}
									}
								}

								@Override
								public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

									if (qName.contains("Position")) {
										inPosition = true;
										logger.debug("found a position!");
										logger.debug(title+"<=== supposed to be the piece title");

									}
									if (inPosition == true) {
										coordinateType = qName;
									}
								}

								@Override
								public void startDocument() {
									try {
										writer = new CSVWriter(writer_, '|', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
									} catch (Throwable t) {
										logger.fatal(t.getMessage(), t);
										System.exit(0);
									}
								}

								@Override
								public void endDocument() {
									try {
										writer.flush();
									} catch (IOException e) {
									}
								}

								private Boolean inRange() {
									Boolean ret = Math.abs(latitude) < 90;
									ret = ret && (Math.abs(longitude) < 180);
									return ret;
								}
							};
					} catch (NullPointerException e1) {
						logger.fatal(e1.getMessage(), e1);
						logger.info(location + " not found, skipping");
						continue;
					}
					try {
						parser.parse(is, handler);
					} catch (SAXException e1) {
						logger.fatal(e1.getMessage(), e1);
					} catch (IOException e1) {
						logger.fatal(e1.getMessage(), e1);
					}
				}
			}
		}
		if (writer_.checkError()) {
			logger.fatal("error occured");
		}
		writer_.flush();
		logger.info("wrote file");
		//FIXME make Scp a pure java thing
		//Scp();
		System.err.printf("Done in %d timeticks", (System.currentTimeMillis()-start));
	}

	public static void Scp() {
		try {
			String lFile = "/tmp/data2.csv";
			String rUser = "hdiwan";
			String rHost = "hasan.d8u.us";
			String rPath = "/home/hdiwan/public_html/newsSrchr/leaflet-simple-csv/data2.csv";
			JSch jsch=new JSch();
			Session session=jsch.getSession(rUser, rHost, 22);
			UserInfo ui=new UserInfo() {
					public String getPassword() { 
						return System.getProperty("remote.password"); 
					}
					public boolean promptYesNo(String str) {
						return true;
					}
					public String getPassphrase(){ 
						return null; 
					}
					public boolean promptPassword(String message){
			       		return true; 
					}
					public void showMessage(String message) {
					}
					public boolean promptPassphrase(String phrase) {
						return true;
					}
			};
			session.setUserInfo(ui);
			session.connect();
		 
			boolean ptimestamp = true;
		   
			// exec 'scp -t rFile' remotely
			String command="scp -t "+rPath;
			Channel channel=session.openChannel("exec");
			OutputStream out=channel.getOutputStream(); 
			InputStream in=channel.getInputStream();
			channel.connect();
			out.write(command.getBytes());
			long filesize=lFile.length();
			command = String.format("C0644 %ld %s\n", filesize, lFile);
			out.write(command.getBytes());
			out.flush();
			FileInputStream fis = new FileInputStream(lFile);
			byte[] buf=new byte[lFile.length() + 1];
			int len=fis.read(buf, 0, buf.length);
			out.write(buf, 0, len);
			out.flush();
			out.close();
			channel.disconnect();
			session.disconnect();
		} catch (Throwable t) {
			logger.fatal(t.getMessage(), t);
		}
	}

	private static String getClassifierPath() {
		String CONF_PROPERTIES = "english.all.3class.distsim.crf.ser.gz";
		return CONF_PROPERTIES;
	}
}
