package us.d8u.news;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 * Process locations and return them as a map of names and pairs of (latitude,
 * longitude)
 *
 * @author hdiwan
 *
 */
public class LocationLookup {
	private static Logger logger = Logger.getLogger(LocationLookup.class);

	protected static final int LATITUDE = 0;
	protected static final int LONGITUDE = 1;
	public static final String URL = "http://geocoder.cit.api.here.com/6.2/geocode.xml?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg&gen=4&searchtext=%s";

	public LocationLookup() {
	}

	public static List<String> extractLocations(String classifyToString) {
		String GOOD_ENTITY_TYPE = "LOCATION";
		String[] parts = classifyToString.split("/");
		List<String> locations = new ArrayList<String>();
		for (int i = 0; i != parts.length; i++) {
			if (parts[i].contains(GOOD_ENTITY_TYPE)) {
				try {
					locations.add(parts[i - 1].split(" ")[1]);
				} catch (ArrayIndexOutOfBoundsException e) {
					logger.debug("Invalid separator for " + parts[i]);
					continue;
				}
			}
		}
		return locations;
	}

	public static List<String> extractLocations(URL url) throws SocketTimeoutException {
		String urlToGet = url.toExternalForm();
		Document document = null;
		try {
			document = Jsoup.connect(urlToGet).get();
		} catch (SocketTimeoutException e) {
			return null;
		} catch (IOException e) {
			logger.fatal(e.getMessage(), e);
			return null;
		}
		// get paragraphs from content
		Elements parapraphs = document.select("p");
		String content = "";
		for (Element element : parapraphs) {
			content += element.text() + " ";
		}
		logger.debug("For " + url.toExternalForm() + " -- got paragraphs containing " + content);
		// get locations from text
		List<String> locations = extractLocations(Main.classifier.classifyToString(content));
		return locations;
	}


}
