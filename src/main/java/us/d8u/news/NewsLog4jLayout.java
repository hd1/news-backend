package us.d8u.news;

import org.apache.log4j.PatternLayout;

public class NewsLog4jLayout extends PatternLayout {
	@Override
	public String getHeader() {
		return "\"Date\", \"Level\", \"Class\", \"Message\"\n";
	}
}
